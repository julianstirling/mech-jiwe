
# Print instructions 


All STLs are in the STL folder of this repo. You need to print the STLS in the orientation they are currently. For some you need to print them both in their current form and mirrored, see below

* 2 x CrossBarMount_p1_Mech-Jiwe.stl
* 2 x CrossBarMount_p2_Mech-Jiwe.stl
* 1 x BearingTopBracket.stl
* 1 x BearingTopBracket.stl - **Mirrored**
* 1 x MotorBracket.stl
* 1 x MotorBracket.stl - **Mirrored**