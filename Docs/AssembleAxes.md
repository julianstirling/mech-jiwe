
# Assembling the axes


{{BOM}}

[needle-nose pliers]: "{Cat: Tool}"
[5mm drill bit]: "{Cat: Tool}"
[8mm spanner]: "{Cat: RecommendedTool}"
[8mm nut driver]: "{Cat: RecommendedTool}"


## Method

### Putting the bearing in part 1



1. Get the [crossbar mount part 1](CrossBarMount_p1_Mech-Jiwe.stl){Qty: 1} and place an [M5 nut]{Qty: 1} into the hex shaped nut trap on the side. 
1. Place an [M5 washer]{Qty: 1} onto a [M5x70 hex head screw]{Qty: 1}, and start to push through the hole on the opposite side from the nut trap until the end is visible in the centre channel.
1. Using [needle-nose pliers]{Qty: 1} place two [M5 nuts][M5 nut]{Qty: 2} onto the end of the screw.
1. Now place a [16mm ball bearing]{Qty: 1} over the screw
1. Finally place another [M5 nut]{Qty: 1} onto the end of the screw. Now there should be three nuts and one bearing on the screw in the centre channel. Getting the last nut in is a little tricky. It involves moving the other nuts with the [needle-nose pliers]{Qty: 1}.
1. Now for the most tedious part of the build: We need to screw the screw into the nut on the far side. For this section we will name the three nuts in the channel Nut 1, Nut 2, and Nut 3 in order they were placed on the screw. Now repeat the following steps
    1. Turn the screw while holding Nut 1 with the [needle-nose pliers]{Qty: 1} until the screw locks.
    2. Use the [needle-nose pliers]{Qty: 1} tighten Nut 2 against Nut 1.
    3. Turn the screw while holding Nut 3 with the [needle-nose pliers]{Qty: 1} until the screw locks.
1. Repeat the above steps until the screw is full inserted into the nut in the nut trap opposite.
1. Tighten the screw until finger tight (stop if it feels like the plastic will break) using an [8mm nut driver]{Qty: 1} or [8mm spanner]{Qty: 1} (if you don't have these you can use the [needle-nose pliers]{Qty: 1}).
1. Tighten the 3 Nuts in the channel to the side of the channel that the screw entered from using an [8mm spanner]{Qty: 1} (if you don't have these you can use the [needle-nose pliers]{Qty: 1}).

### Putting the bearings in part 2

1. Get a [M5x20 hex head screw]{Qty: 4} (cannot be longer than 23mm or will interfere) and place an [M5 washer]{Qty: 4} on the end.
1. Put this screw into one of the four holes on the slanted faces on the back of the [crossbar mount part 2]{Qty: 1}, until the end protrudes into the square cavity.
1. Place an [M5 washer]{Qty: 4} onto this screw and then a [16mm ball bearing]{Qty: 4} onto the screw.
1. Finally add an [M5 nut]{Qty: 4} onto the screw.
1. Place an [M5 nut]{Qty: 4} into the nut trap on opposite the screw.
1. Tighten the screw into the nut in the trap using an [8mm nut driver]{Qty: 1} or [8mm spanner]{Qty: 1} (if you don't have these you can use the [needle-nose pliers]{Qty: 1}). You may have to hold the nut in the bearing cavity with another spanner or pliers.
1. Once the screw is tight, tighten the nut so it pushes the bearing against the washer

### Connecting it all together

1. Using a [5mm drill bit]{Qty: 1} clean out the final 4 remaining holes of [crossbar mount part 1] as and check that an [M5x50 hex head screw]will go all the way trough.
1. Use the [needle-nose pliers]{Qty: 1} to clean out the nut traps for these 4 holes
1. Using a [5mm drill bit]{Qty: 1} clean out the final 4 remaining holes of [crossbar mount part 2] as and check that an [M5x50 hex head screw] will go all the way trough.
1. Place an [M5 nut]{Qty: 4} in each of the nut traps and push home with [needle-nose pliers]{Qty: 1}
1. Stand [crossbar mount part 1] and [crossbar mount part 2] on their ends next to each other.
1. Place four [M5 washers][M5 washer]{Qty: 4} and onto four [M5x50 hex head screws][M5x50 hex head screw]{Qty: 4} (longer works too) and push them into part 2 and start tightening them into the nuts in part 1.
1. Place a [20mm piece of Stainless Steel box section]{Qty: 1} though the square gap between the pieces.
1. Tighten screws until the steel box section runs smoothly

### Repeat
Repeat this whole process for the second axis.