The python software for running the Mech-Jiwe mechanical tester

## Installation

It is best to use Git to clone rather than download the repository as this will allow you to update it in place.

If you navigate to this directory in a terminal. You can then install it by running the following line of code

    pip install -e .
    
If you have no errors you should now be able to run the following command

    mechjiwe

This should run the Mech-Jiwe terminal software.

### Updating the software

If you cloned the repository with git and installed the software with the command above, you should be able to run

    git pull
 
 anywhere in the repository to update the software.
 
 
# Running the software

The Mech-Jiwe software is a text based program with menus.

When in the main menu you can move the tester with the up (or `w`) and down (or `s`) keys. This can be used to set up experiments. To set how far it moved per key-press enter the setup menu with `m`.

The setup menu can be used to:

 * Set the number of steps moved when arrow keys are pressed
 * Set the output directory
 * Set the sample ID
 
You can read out the current load with `l` or zero the load with `t`.

`r` will run an experiment (see below for more details)

`q` will quit the software

## Experiments

When you press `r` the experiment menu will open. If you have not yet set a sample ID, the software will prompt you for a sample ID.

There are three types of experiment **Pull**, **Cycle**, and **Increasing cycle**, each are described below. Experiments can be stopped mid run by pressing `t`.

A positive distance refers to the tester moving up.

### Pull

Pull will simply move the tester until either a maximum distance is reached or the tester detects the piece has broken.

The tester will ask for four pieces of information

1. The distance to be moved (in mm) between individual force measurements.
1. The total distance that the tester will move for this test.
1. Number of averages for each force measurement
1. The time (in seconds) between each force measurement (0 to run as fast as the tester can move).

For example if you enter:


    Set step size between measurements in mm (positive=up) : 1
    Set total distance in mm: 5
    Number of averages for each measurement: 5
    Time per point (enter nothing to run as fast as possible): 1

The tester will take force readings at the positions. 0 mm, 1 mm, 2 mm, 3 mm, 4 mm, and 5 mm. It will quickly average 5 force measurements at each point. The total experiment will take about five seconds as the tester will try to take 1 second moving between each point.

### Cycle

Cycle is similar to pull except that it moves up to the total distance set and then back down to the original position. Also it will not detect if the piece has broken.

All inputs are the same as pull except there is one extra input:

* The number of cycles to perform. This is the number of times it moves up and down

For example if you enter:

    Set step size between measurements in mm (positive=up) : 1
    Set total distance in mm: 5
    Number of cycles to perform: 2
    Number of averages for each measurement: 5
    Time per point (enter nothing to run as fast as possible): 1

The Mech-Jiwe will take measurements at the positions:  
[0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0] mm, there will still be about 1 second between each of these positions.

### Increasing cycle

Increasing cycle performs multiple cycle measurements with different total distances. The total distance set is the total distance of the last cycle, the size of cycles are equally spaced/

One extra parameter is asked for.

* The number of increments. This is how many cycle experiments are performed.

For example if you enter:

    Set step size between measurements in mm (positive=up) : 1
    Set total distance in mm: 6
    Number of cycles to perform: 2
    Number of increments to perform: 3
    Number of averages for each measurement: 5
    Time per point (enter nothing to run as fast as possible): 1

*Note that total distance has changed to 6 here*

The Mech-Jiwe is will effectively take three cycle measurements one with a total distance of 2 mm, one with a total distance of 4 mm, and one with a total distance of 6mm. i.e.  
[0, 1, 2, 1, 0, 1, 2, 1, 0,  
1, 2, 3, 4, 3, 2, 1, 0, 1, 2, 3, 4, 3, 2, 1, 0  
1, 2, 3, 4, 5, 6, 5, 4, 3, 2 1, 0, 1, 2, 3, 4, 5, 6, 5, 4, 3, 2 1, 0] mm

The new lines are only to emphases the different experiments.
