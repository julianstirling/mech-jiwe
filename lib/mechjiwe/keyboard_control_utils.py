
"""
Keyboard controls utilities for the mech-jiwe.

This is based on the old command line interface for the OpenFlexure microscope
written by Richard Bowman, Abhishek Ambekar, James Sharkey and Darryl Foo

Substantially rewritten by Richard Bowman and Julian Stirling for the Mech Jiwe

Released under GNU GPL v3 or later.

"""

import numpy as np

class InteractiveParameter(object):
    """This class is intended to allow a setting to be easily controlled.

    The basic version allows the value to be picked from a list.
    """
    _value = None
    name = ""
    allowed_values = []
    wrap = False

    def __init__(self, name, allowed_values, wrap=False, initial_value=None, readonly=False):
        """Create an object to manage a parameter.

        name: the name of the setting
        allowed_values: a list of values which are permitted
        wrap: whether incrementing past the end wraps to the start.
        """
        self.name = name
        self.allowed_values = allowed_values
        self.wrap = wrap
        self.readonly = readonly
        if initial_value is None:
            self._value = allowed_values[0]
        else:
            self._value = initial_value

    @property
    def value(self):
        """The value of the property we're manipulating"""
        return self._value

    @value.setter
    def value(self, newvalue):
        if not self.readonly:
            self._value = newvalue
        else:
            print("Warning: {} is a read-only property.".format(self.name))

    def current_index(self):
        """The index (in the allowed_values list) of the current value."""
        try:
            return list(self.allowed_values).index(self.value)
        except ValueError:
            try:
                allowed = np.array(self.allowed_values)
                return np.argmin((allowed - float(self.value))**2)
            except:
                print("Warning: the value of {} was {}, which is neither "
                      "allowed nor numerical!".format(self.name, self.value))
                return 0

    def change(self, step):
        """Change the value of this property by +/- 1 step"""
        assert step in [-1, 1], "Step must be in [-1, 1]"
        if self.readonly:
            return  # don't change the property if we can't change it!
        i = self.current_index() + step
        N = len(self.allowed_values)
        if self.wrap:
            i = (i + N) % N  # this ensures we wrap if i would be invalid
        if i >= 0 and i < N:
            self.value = self.allowed_values[i]


class FunctionParameter(InteractiveParameter):
    def __init__(self, name, function, value_function=None, args=None, kwargs=None, step_kwarg_name=None):
        """Create a 'parameter' to run a function.

        name: the name of the function
        function: the callable to run
        args, kwargs: arguments for the above
        """
        self.name = name
        self.function = function
        if args is None:
            args = []
        self.f_args = args
        if kwargs is None:
            kwargs = {}
        self.f_kwargs = kwargs
        self.step_kwarg_name = step_kwarg_name
        self.value_function = value_function

    @property
    def value(self):
        if self.value_function() is None:
            return "Press: + or -"
        else:
            return self.value_function()

    @value.setter
    def value(self):
        print("Cannot set the value of a function parameter")

    def current_index(self):
        return 0

    def change(self, step):
        if self.step_kwarg_name is not None:
            self.f_kwargs[self.step_kwarg_name] = step
        self.function(*self.f_args, **self.f_kwargs)


class ReadOnlyObjectParameter(InteractiveParameter):
    """A dummy InteractiveParameter that only reads things."""

    def __init__(self, obj, name, filter_function=lambda x: x):
        """Create a dummy parameter that reads a value from an object."""
        self.obj = obj
        self.filter_function = filter_function
        InteractiveParameter.__init__(self, name, [None], readonly=True)

    @property
    def value(self):
        return self.filter_function(getattr(self.obj, self.name))

    def current_index(self):
        return 0

    def change(self, d):
        pass


class ReadOnlyObjectGetterParameter(ReadOnlyObjectParameter):
    """A dummy InteractiveParameter that only reads things.  

    Uses a getter function (no args) rather than a property"""
    @property
    def value(self):
        return self.filter_function(getattr(self.obj, self.name)())
