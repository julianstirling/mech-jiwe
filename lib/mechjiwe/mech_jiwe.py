import matplotlib.pyplot as plt
import serial
import time
import tempfile
import webbrowser
import os
import tempfile
from datetime import datetime
import numpy as np

import matplotlib as mpl
mpl.use('Agg')


def readdict(dictionary, param):
    if param in dictionary:
        return dictionary[param]
    else:
        return None


class MechJiwe(object):

    def __init__(self, port, steps_per_rev=200, dist_per_rev_mm=1.):
        self.port = port
        self.ser = serial.Serial(port)
        self.abortrun = False
        self.pos = 0
        self.steps_per_rev = steps_per_rev
        self.dist_per_rev_mm = dist_per_rev_mm

    def get_tester_properties(self):
        return {'tester': 'Mech-Jiwe', 'port': self.port, 'steps_per_rev': self.steps_per_rev, 'dist_per_rev_mm': self.dist_per_rev_mm}

    @property
    def pos_mm(self):
        return (self.pos/self.steps_per_rev)*self.dist_per_rev_mm

    def write(self, cmd):
        self.ser.write(cmd.encode())

    def clear_buffer(self):
        self.ser.read(self.ser.inWaiting())

    def move(self, steps):
        assert type(steps) is int, 'Steps must be an interger'
        if steps == 0:
            pass
        else:
            self.write('move %d\r' % steps)
            self.pos += steps
        tmp = self.ser.readline()

    def set_speed(self, RPM):
        assert type(RPM) is int, 'Speed (RPM) must be an interger'
        assert RPM > 0, 'Speed (RPM) must be larger than 0'
        self.write('set_speed %d\r' % RPM)
        tmp = self.ser.readline()

    def load(self, raw=False):
        '''returns load in mN'''
        self.clear_buffer()
        if raw:
            self.write('load_raw \r')
        else:
            self.write('load \r')
        F = float(self.ser.readline())
        return F

    def tare(self):
        self.clear_buffer()
        self.write('tare\r')
        tmp = self.ser.readline()

    def run_experiment(self, experiment_data, show=False, verbose=False):
        self.abortrun = False

        exp_type = readdict(experiment_data, 'type')

        dist_mm = readdict(experiment_data, 'total_distance_mm')
        step_dist_mm = readdict(experiment_data, 'step_distance_mm')
        dist_steps = (dist_mm/self.dist_per_rev_mm) * self.steps_per_rev
        step_size = int((step_dist_mm/self.dist_per_rev_mm)
                        * self.steps_per_rev)

        point = 0
        runs = []

        if exp_type is None:
            print('No experiment type given')
            return {}

        elif exp_type.lower() == 'pull':

            num = int(abs(dist_steps//step_size))
            steps = [0]+[step_size]*num
            runs.append({'start': point,
                         'end': point+num})
            point += num

            experiment_data['steps'] = steps
            experiment_data['runs'] = runs
            return self.perform_experiment(experiment_data, show=show, verbose=verbose)

        elif exp_type.lower() == 'cycle':

            n_cyc = data['number_cycles']
            num = int(abs(dist_steps//step_size))
            steps = [0]

            for n in range(n_cyc):
                steps += [step_size]*num
                runs.append({'start': point,
                             'end': point+num})
                point += num
                steps += [-step_size]*num
                runs.append({'start': point,
                             'end': point+num})
                point += num

            experiment_data['steps'] = steps
            experiment_data['runs'] = runs
            return self.perform_experiment(experiment_data, show=show, verbose=verbose)

        elif exp_type.lower() == 'increasing_cycle':

            n_cyc = data['number_cycles']
            n_inc = data['number_increments']
            steps = [0]

            for n in range(n_inc):
                num = int(abs((dist_steps*(n+1)/n_inc)//step_size))
                for n in range(n_cyc):
                    steps += [step_size]*num
                    runs.append({'start': point,
                                 'end': point+num})
                    point += num
                    steps += [-step_size]*num
                    runs.append({'start': point,
                                 'end': point+num})
                    point += num

            experiment_data['steps'] = steps
            experiment_data['runs'] = runs
            return self.perform_experiment(experiment_data, show=show, verbose=verbose)
        else:
            print(f'Experiment type {exp_type} not recognised')
            return {}

    def perform_experiment(self, experiment_data, show=False, verbose=False):

        steps = readdict(experiment_data, 'steps')

        exp_type = readdict(experiment_data, 'type')

        time_per_point = readdict(experiment_data, 'time_per_point')
        if time_per_point is None:
            control_speed = False
        else:
            control_speed = True
            try:
                time_per_point = float(time_per_point)
                assert time_per_point > 0
            except:
                raise RuntimeError(
                    "time_per_point should be a positive number")

        if readdict(experiment_data, 'tare'):
            self.tare()

        n_av = readdict(experiment_data, 'number_averages')

        auto_stop = readdict(experiment_data, 'auto_stop')

        ps = []
        Fs = []
        ts = []
        Fs_std = []
        time_warned = False
        n_zero = 0
        end_condition = 'Experiment completed'
        max_Fs = 0
        with open(os.path.join(tempfile.gettempdir(), experiment_data['time_str']+"--"+experiment_data['sample_id']+"__MJ_data.txt"), 'w') as fid:
            for n, step in enumerate(steps):
                if not n == 0:
                    self.move(step)
                ps.append(self.pos_mm)

                if not n == 0:
                    t0 = time.time()
                    if control_speed:
                        td = t+time_per_point-t0
                        if td > 0:
                            time.sleep(td)
                        elif not time_warned:
                            print(
                                'Warning: requested speed could not be maintained\n\r')
                            time_warned = True
                t = time.time()
                ts.append(t)
                F = []
                for m in range(n_av):
                    F.append(self.load()/1000.0)
                fid.write(str(F)[1:-1]+"\n")
                if n_av > 2:
                    poplist = []
                    for m in range(n_av):
                        other_vals = F[:m]+F[m+1:]
                        others_m = np.mean(other_vals)
                        others_std = np.std(other_vals, ddof=1)
                        # remove anything which is outside 10 standard deviations rest of the data
                        if (abs(F[m]-others_m)/others_std) > 10:
                            poplist.append(m)
                    # reverse so that multiple cannot be popped
                    for p in reversed(poplist):
                        F.pop(p)

                Fs.append(float(np.mean(F)))
                max_Fs = max(max_Fs, abs(Fs[-1]))
                Fs_std.append(float(np.std(F, ddof=1)))

                if verbose:
                    print("Measurement %d of %d: F = %.2g N\n\rPress t to terminate!\n\r" % (
                        n+1, len(steps), F[-1]))
                if auto_stop:
                    if max_Fs > 100*abs(Fs[-1]):
                        n_zero += 1
                    else:
                        n_zero = 0
                    if n_zero > 4:
                        end_condition = 'Auto stop: zero'
                        break
                if self.abortrun:
                    end_condition = 'User aborted'
                    break

        if show:
            plt.clf()
            if (exp_type.lower() == 'cycle') or (exp_type.lower() == 'increasing_cycle'):
                plt.plot(ps, Fs, 'b--')
            plt.plot(ps, Fs, 'r.')
            plt.xlabel('Distance (mm)')
            plt.ylabel('Force (N)')
            plt.tight_layout()
            now = datetime.now()

            filename = os.path.join(tempfile.gettempdir(
            ), "MechJiwe_"+now.strftime("%Y%m%d_%H%M%S")+".png")
            plt.savefig(filename)
            webbrowser.open(filename)

        data_dict = {"n_average": n_av,
                     "position": ps,
                     "load": Fs,
                     "load_std": Fs_std,
                     "time": ts,
                     "tester": self.get_tester_properties(),
                     "end_condition": end_condition
                     }

        for key in experiment_data:
            data_dict[key] = experiment_data[key]

        return data_dict
